package com.merttutsakgmail.dovizkur.dialogs;

import android.app.Dialog;
import android.widget.TextView;
import android.widget.ProgressBar;

import com.merttutsakgmail.dovizkur.R;
import com.merttutsakgmail.dovizkur.activities.BaseActivity;
import com.merttutsakgmail.dovizkur.activities.GirisActivity;


public class ProgressDialog{

    private Dialog pDialog;
    private BaseActivity baseActivity;
    private GirisActivity girisActivity;

    public  ProgressDialog(BaseActivity baseActivity){

        this.baseActivity = baseActivity;

        this.pDialog = new Dialog(this.baseActivity);
    }
    public  ProgressDialog(GirisActivity girisActivity){

        this.girisActivity = girisActivity;

        this.pDialog = new Dialog(this.girisActivity);
    }

    public void onCreateProgressDialog(String metin) {



        pDialog.setContentView(R.layout.progres_dialog);

        final TextView textView = (TextView) pDialog.findViewById(R.id.textViewProgressMetin);
        final ProgressBar progressBar = (ProgressBar) pDialog.findViewById(R.id.progressBar);

        progressBar.setActivated(true);
        textView.setText(metin);

        pDialog.show();

    }

    public void dismissProgressDialog() {

        pDialog.cancel();
        pDialog.dismiss();

    }
}


