package com.merttutsakgmail.dovizkur.activities;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.merttutsakgmail.dovizkur.R;
import com.merttutsakgmail.dovizkur.dialogs.ProgressDialog;
import com.merttutsakgmail.dovizkur.fragments.BaseFragment;
import com.merttutsakgmail.dovizkur.fragments.SettingsFragment;

public class BaseActivity extends FragmentActivity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private String a;
    private ImageButton backButton;
    private ImageButton settingsButton;
    private TextView baslikTextview;
    private String isExit = "";
    private ProgressDialog progressDialog;
    private int settingsteTutulanEleman = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        backButton = (ImageButton) findViewById(R.id.header_left);
        settingsButton = (ImageButton) findViewById(R.id.header_right);
        baslikTextview = (TextView) findViewById(R.id.header_text);

        preferences = super.getSharedPreferences("tercihlerim", Context.MODE_PRIVATE);
        editor = preferences.edit();

        progressDialog = new ProgressDialog(BaseActivity.this);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new SettingsFragment());
            }
        });
    }

    private void changeFragment(BaseFragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).addToBackStack(fragment.toString()).commit();
        Log.d("STACK", "PUSH" + fragment.toString());
    }

    protected void initView(BaseFragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).addToBackStack(fragment.toString()).commit();
        Log.d("STACK", "PUSH" + fragment.toString());


    }

    public void visibilityButton(boolean left, boolean mid, boolean right) {

        if (left == true) {
            backButton.setVisibility(View.VISIBLE);
        } else {
            backButton.setVisibility(View.GONE);
        }

        if (mid == true) {
            baslikTextview.setVisibility(View.VISIBLE);
        } else {
            baslikTextview.setVisibility(View.GONE);
        }

        if (right == true) {
            settingsButton.setVisibility(View.VISIBLE);
        } else {
            settingsButton.setVisibility(View.GONE);
        }

    }

    public void midButtonText(String text) {
        baslikTextview.setText(text);
    }

    public void isExit(String exit) {
        this.isExit = exit;
    }

    public void cameFromSettings(int settingsteTutulanEleman, String exit) {
        this.settingsteTutulanEleman = settingsteTutulanEleman;
        isExit(exit);
    }

    @Override
    public void onBackPressed() {

        if (isExit == "SECILI_OLAN_DOVIZ") {
            kapatDialog().show();
        } else if (isExit == "BASLANGIC") {
            kapatDialog().show();
        } else if (isExit == "SETINGS_SECILI_OLMAYAN") {
            setSeciliEleman(settingsteTutulanEleman);
            Log.d("STACK", "POP");
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }


    private Dialog kapatDialog() {
        final Dialog dialog = new Dialog(BaseActivity.this);
        dialog.setContentView(R.layout.dialog_tercih);

        final TextView metin = (TextView) dialog.findViewById(R.id.textViewMetin);
        final Button button_positive = (Button) dialog.findViewById(R.id.buttonPositive);
        final Button button_negative = (Button) dialog.findViewById(R.id.buttonNegative);

        metin.setText("Programdan ayrılmak istediğinize emin misiniz ?");


        button_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("STACK", "POP");
                finish();
            }
        });

        button_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public boolean internetErisimi() {

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }


    void setSeciliEleman(int elemanIndex) {
        editor.putInt("SECILEN", elemanIndex).apply();
    }
}
