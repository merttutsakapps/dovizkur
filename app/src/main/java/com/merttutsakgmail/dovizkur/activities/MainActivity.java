package com.merttutsakgmail.dovizkur.activities;

import android.os.Bundle;

import com.merttutsakgmail.dovizkur.fragments.ListFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isExit("BASLANGIC");
        initView(new ListFragment());
    }
}
