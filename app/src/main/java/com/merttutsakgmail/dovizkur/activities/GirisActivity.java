package com.merttutsakgmail.dovizkur.activities;


import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.merttutsakgmail.dovizkur.R;
import com.merttutsakgmail.dovizkur.dialogs.ProgressDialog;

public class GirisActivity extends AppCompatActivity {

    private Button buttonInternet;
    private TextView ınternetTextview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giris);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        final ProgressDialog progressDialog = new ProgressDialog(GirisActivity.this);

        buttonInternet = (Button) findViewById(R.id.buttonInternet);
        ınternetTextview = (TextView) findViewById(R.id.textInternet);

        if (internetErisimi()) {
            {

                ınternetTextview.setText("");
                buttonInternet.setVisibility(View.GONE);
                appOpen();
            }
        } else {
            {
                buttonInternet.setVisibility(View.VISIBLE);
                ınternetTextview.setText("İnternet bağlantısını bulunamadı !");
            }

        }

        buttonInternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (internetErisimi()) {
                    {

                        progressDialog.onCreateProgressDialog("İşleminiz yürütülüyor... Lütfen bekleyiniz...");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismissProgressDialog();
                                buttonInternet.setVisibility(View.GONE);
                                ınternetTextview.setText("İnternet bağlantınız sağlandı. Uygulamanız açılıyor..");
                                appOpen();
                            }}, 1500);
                    }
                } else {
                    {
                        buttonInternet.setVisibility(View.VISIBLE);
                        Toast.makeText(GirisActivity.this, "İnternet bağlantınız şuanda aktif değil! tekrar deneyiniz.", Toast.LENGTH_SHORT).show();
                        ınternetTextview.setText("İnternet bağlantısını bulunamadı !");
                    }
                }
            }
        });
    }

    private boolean internetErisimi() {

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {return false;}
    }

    private void appOpen(){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(GirisActivity.this, MainActivity.class);
                startActivity(intent);
                GirisActivity.this.finish();
            }
        }, 2000);
    }
}
