package com.merttutsakgmail.dovizkur.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.merttutsakgmail.dovizkur.HttpHandler.HttpHandler;
import com.merttutsakgmail.dovizkur.R;
import com.merttutsakgmail.dovizkur.activities.BaseActivity;
import com.merttutsakgmail.dovizkur.dialogs.ProgressDialog;
import com.merttutsakgmail.dovizkur.views.adapters.CustomListAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ListFragment extends BaseFragment {


    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private String[] kKur;
    private String[] uKur;
    private String[] sKur;
    private String[] listeItem;
    private String[] yKKur;
    private String[] yUKur;
    private String[] ySKur;
    private String value;
    private double[] oran;
    private boolean dialogDegisimDurum;

    private ListView liste;
    private BaseActivity baseActivity;
    private ProgressDialog pDialog;
    private CustomListAdapter adapter;
    private TextView baseTextview;
    private TextView dateTextview;
    private PullRefreshLayout pullRefreshLayout;
    private LinearLayout baseLinearLayout;
    private RelativeLayout aciklamaLinearLayout;

    ProgressDialog progressDialog;

    private static final String TAG = HttpHandler.class.getSimpleName();
    private static String url1 = "";
    private static String urlbase = "";
    private static String urldate = "";

    private ArrayList<HashMap<String, String>> contactList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list, container, false);

        kKur = getResources().getStringArray(R.array.kKur);
        uKur = getResources().getStringArray(R.array.uKur);
        sKur = getResources().getStringArray(R.array.sKur);

        yKKur = new String[kKur.length];
        yUKur = new String[uKur.length];
        ySKur = new String[uKur.length];

        baseActivity = (BaseActivity) getActivity();

        progressDialog = new ProgressDialog(baseActivity);

        baseTextview = (TextView) view.findViewById(R.id.baseText);
        dateTextview = (TextView) view.findViewById(R.id.dateText);
        pullRefreshLayout = (PullRefreshLayout) view.findViewById(R.id.pulRefreshLayout);
        //pullRefreshLayout.setColor(Color.BLUE);
        liste = (ListView) view.findViewById(R.id.list_fragment);
        baseLinearLayout = (LinearLayout) view.findViewById(R.id.baseLinerLayout);
        aciklamaLinearLayout = (RelativeLayout) view.findViewById(R.id.aciklamaLayout);
        preferences = baseActivity.getSharedPreferences("tercihlerim", Context.MODE_PRIVATE);
        editor = preferences.edit();

        baseActivity.visibilityButton(false, true, false);
        baseActivity.midButtonText("Döviz Kuru");

        contactList = new ArrayList<>();

        baseTextControl("", "");

        if (getSeciliEleman() != -1) {
            Log.d("onCreateView", "Eleman -1'e girmedi.");
            try {
                new GetContacts(baseActivity).execute();
            } catch (Exception e) {
            }
        } else {
            Log.d("onCreateView", " Eleman -1 geldi ama false.");
            listele();
        }

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                if (getSeciliEleman() == -1) {
                    onCreateDialog("SECILMEMIS_DIALOG", arg2);
                } else {
                    onCreateDialog("SECILMIS_DIALOG", arg2);
                }
            }
        });
        pullRefreshLayout.setRefreshStyle(pullRefreshLayout.STYLE_SMARTISAN);

        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                pullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (baseActivity.internetErisimi() == true) {
                            listele();
                        } else {
                            Toast.makeText(baseActivity, "Bilgileri güncellemeniz için interneti açmanız gerekmektedir.", Toast.LENGTH_LONG).show();
                        }
                        pullRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        return view;
    }

    private String urlOlustur() {
        if (getSeciliEleman() == -1) {
            Log.e(TAG, "Yanlış url");
        } else {
            urldate = getDATE();
            urlbase = "?base=" + kKur[getSeciliEleman()];
        }

        url1 = "http://api.fixer.io/";
        Log.e("URL", url1 + urldate + urlbase);

        return url1 + urldate + urlbase;
    }

    void listele() {
        listeItem = new String[kKur.length];
        for (int i = 0; i < kKur.length; i++) {
            listeItem[i] = kKur[i] + "(" + uKur[i] + ")";
        }

        if (getSeciliEleman() == -1) {
            Log.d("LİSTE", "Secilen eleman -1 oldu");
            UpLayoutControl(false);
            pullRefreshLayout.setEnabled(false);

            baseActivity.visibilityButton(false, true, false);

            ArrayAdapter arrayAdapter = new ArrayAdapter(baseActivity, R.layout.list_item, R.id.item_text_view, listeItem);
            liste.setAdapter(arrayAdapter);
        } else {
            baseActivity.isExit("SECILI_OLAN_DOVIZ");
            UpLayoutControl(true);
            pullRefreshLayout.setEnabled(true);
            oran = new double[contactList.size()];

            int j = 0;
            for (int i = 0; i < yKKur.length; i++) {
                if (kKur[getSeciliEleman()] != kKur[i]) {
                    oran[j] = Double.parseDouble(contactList.get(j).get(kKur[i]));
                    j++;
                }
            }

            baseActivity.visibilityButton(false, true, true);

            adapter = new CustomListAdapter(baseActivity, yKKur, yUKur, sKur[getSeciliEleman()], oran);
            liste.setAdapter(adapter);
        }

    }

    void setSeciliEleman(int elemanIndex) {
        if (elemanIndex != -1) {
            baseActivity.visibilityButton(false, true, true);
        }

        editor.putInt("SECILEN", elemanIndex).apply();
    }

    int getSeciliEleman() {
        return preferences.getInt("SECILEN", -1);
    }

    private class GetContacts extends AsyncTask<Void, Void, Void> {
        private BaseActivity baseActivity;

        public GetContacts(BaseActivity activity) {
            baseActivity = activity;
        }

        //HTTP'e bağlanmadan önce ki olaylar
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("onPreExecute", "GİRDİ");
            //progress dialog göstericek
            progressDialog.onCreateProgressDialog("İşleminiz yürütülüyor... Lütfen bekleyiniz...");
            Log.d("onPreExecute", "ÇIKTI");
        }


        //HTTP'e bağlandıktan sonra ki olaylar
        @Override
        protected Void doInBackground(Void... arg0) {
            Log.d("doInBackground", "GİRDİ");
            HttpHandler sh = new HttpHandler();

            String jsonStr = sh.makeServiceCall(urlOlustur());
            Log.e(TAG, "Response from rl: " + jsonStr);

            if (jsonStr != null) {
                if ((getSeciliEleman() != -1)) {
                    try {
                        Log.d("GETCONTACTS", "-1 işlemi");
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        JSONObject rates = jsonObj.getJSONObject("rates");


                        int j = 0;

                        for (int i = 0; i < kKur.length; i++) {
                            if (kKur[getSeciliEleman()] != kKur[i]) {
                                double kurOran = rates.getDouble(kKur[i]);

                                yKKur[j] = kKur[i];
                                yUKur[j] = uKur[i];
                                ySKur[j] = sKur[i];

                                j++;

                                HashMap<String, String> contact = new HashMap<>();

                                //HASHMAP anahtarına tüm child düğümlerini ekliyoruz. => value
                                contact.put(kKur[i], String.valueOf(kurOran));

                                //contact'ı contact list'e ekiyoruz.
                                contactList.add(contact);
                            } else {
                                Log.e(TAG, kKur[getSeciliEleman()] + " - " + kKur[i]);
                            }
                        }

                        final String date = jsonObj.getString("date");
                        final String base = uKur[getSeciliEleman()] + " (" + jsonObj.getString("base") + ")";

                        baseActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                baseTextControl(base, date);
                                listele();
                            }
                        });
                    } catch (final JSONException e) {
                        Log.e(TAG, "Json parsing error: " + e.getMessage());
                    }
                }
            } else {

                Log.d("GETCONTACTS", "-1 işlemi");
                Log.e(TAG, "Couldn't get json from server.");
            }
            Log.d("doInBackground", "ÇIKTI");
            return null;
        }

        //HTTP'e bağlandıktan sonra ki olaylar
        @Override
        protected void onPostExecute(Void result) {
            Log.d("onPostExecute", "GİRDİ");
            super.onPostExecute(result);
            progressDialog.dismissProgressDialog();
            Log.d("onPostExecute", "ÇIKTI");
        }
    }

    protected void onCreateDialog(final String tercih, final int secilenİndex) {
        Dialog dialog = null;
        if (tercih == "SECILMEMIS_DIALOG") {
            dialog = secilmemisDialog(secilenİndex);
        } else if (tercih == "SECILMIS_DIALOG") {
            dialog = secilmisDialog(secilenİndex);
        }
        dialog.show();
    }

    private Dialog secilmemisDialog(final int seciliİndex) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.dialog_tercih);

        final TextView metin = (TextView) dialog.findViewById(R.id.textViewMetin);
        final Button button_positive = (Button) dialog.findViewById(R.id.buttonPositive);
        final Button button_negative = (Button) dialog.findViewById(R.id.buttonNegative);


        metin.setText(uKur[seciliİndex].toString() + "(" + kKur[seciliİndex].toString() + ") kurunu seçmek istediğinizden emin misiniz ?");


        button_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSeciliEleman(seciliİndex);
                new GetContacts(baseActivity).execute();
                dialog.dismiss();
            }
        });

        button_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    private Dialog secilmisDialog(final int seciliİndex) {
        Log.d("DİALOG", "SECİLMİS");
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.dialog_kur);

        final TextView ustKurText = (TextView) dialog.findViewById(R.id.ustKurText);
        final EditText ustKurEdit = (EditText) dialog.findViewById(R.id.ustKurEdit);
        final TextView altKurText = (TextView) dialog.findViewById(R.id.altKurText);
        final EditText altKurEdit = (EditText) dialog.findViewById(R.id.altKurEdit);
        final Button degistir = (Button) dialog.findViewById(R.id.degistirButton);
        final ImageButton temizle = (ImageButton) dialog.findViewById(R.id.temizleButton);

        ustKurText.setText(uKur[getSeciliEleman()] + " (" + kKur[getSeciliEleman()] + ")");
        altKurText.setText(yUKur[seciliİndex] + " (" + yKKur[seciliİndex] + ")");

        hintYaz(dialog, seciliİndex);

        altKurEdit.setEnabled(false);
        ustKurEdit.setEnabled(true);
        kurBelirle(dialog, seciliİndex);

        dialogDegisimDurum = false;

        ustKurEdit.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                if (ustKurEdit.getText().toString() == "") {
                    altKurEdit.setText("");
                }
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                try {
                    if (!dialogDegisimDurum) {
                        value = String.format("%.4f", Double.parseDouble(s.toString()) * oran[seciliİndex]);
                        kurBelirle(dialog, seciliİndex);
                        altKurEdit.setText(value);
                    } else {
                        value = String.format("%.4f", Double.parseDouble(s.toString()) * (1 / oran[seciliİndex]));
                        kurBelirle(dialog, seciliİndex);
                        altKurEdit.setText(value);
                    }

                } catch (Exception e) {

                    baseActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            altKurEdit.setText("");
                        }
                    });

                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                }
            }

            public void afterTextChanged(Editable s) {
            }
        });

        degistir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tmpText = altKurText.getText().toString();
                String tmpEdit = altKurEdit.getText().toString();
                altKurEdit.setText(ustKurEdit.getText());
                altKurText.setText(ustKurText.getText());

                ustKurEdit.setText(tmpEdit);
                ustKurText.setText(tmpText);

                if (dialogDegisimDurum == false) {
                    dialogDegisimDurum = true;
                } else {
                    dialogDegisimDurum = false;
                }

                kurBelirle(dialog, seciliİndex);

                hintYaz(dialog, seciliİndex);

            }
        });

        temizle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                altKurEdit.setText("");
                ustKurEdit.setText("");
            }
        });
        return dialog;
    }


    private void kurBelirle(Dialog dialog, int seciliİndex) {
        final TextView sembolUstTextview = (TextView) dialog.findViewById(R.id.sembolKurUstText);
        final TextView sembolAltTextview = (TextView) dialog.findViewById(R.id.sembolKurAltText);

        if (!dialogDegisimDurum) {
            sembolUstTextview.setText(sKur[getSeciliEleman()]);
            sembolAltTextview.setText(ySKur[seciliİndex]);
        } else {
            sembolUstTextview.setText(ySKur[seciliİndex]);
            sembolAltTextview.setText(sKur[getSeciliEleman()]);
        }

    }

    private void hintYaz(Dialog dialog, int seciliİndex) {

        final EditText ustKurEdit = (EditText) dialog.findViewById(R.id.ustKurEdit);
        final EditText altKurEdit = (EditText) dialog.findViewById(R.id.altKurEdit);

        altKurEdit.setText("");
        ustKurEdit.setText("");

        ustKurEdit.setHint("1");
        if (!dialogDegisimDurum) {
            altKurEdit.setHint(String.valueOf(oran[seciliİndex]));
        } else {
            altKurEdit.setHint(String.valueOf(1 / oran[seciliİndex]));
        }

    }

    public void baseTextControl(String base, String date) {
        if (getSeciliEleman() != -1) {
            if ((base != "") && (date != "")) {
                UpLayoutControl(true);

                dateTextview.setText("Döviz Tarihi : " + date);
                baseTextview.setText("Temel Para Birimi : " + base);
            } else {
                UpLayoutControl(false);

                dateTextview.setText("Döviz Tarihi : " + date);
                baseTextview.setText("Temel Para Birimi : " + base);
            }
        } else {
            UpLayoutControl(false);
        }
    }

    public void UpLayoutControl(boolean state) {
        if (!state) {
            aciklamaLinearLayout.setVisibility(View.VISIBLE);
            baseLinearLayout.setVisibility(View.GONE);
        } else {
            aciklamaLinearLayout.setVisibility(View.GONE);
            baseLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    int getYear() {
        return preferences.getInt("YEAR", Calendar.getInstance().get(Calendar.YEAR));
    }

    int getMonth() {
        return preferences.getInt("MONTH", Calendar.getInstance().get(Calendar.MONTH)+1);
    }

    int getDay() {
        return preferences.getInt("DAY", Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
    }

    String getDATE() {
        String month;
        String day;
        String year;

        if (getYear() < 10) {
            year = "0" + getYear();
        } else {
            year = String.valueOf(getYear());
        }
        if (getMonth() < 10) {
            month = "0" + getMonth();
        } else {
            month = String.valueOf(getMonth());
        }
        if (getDay() < 10) {
            day = "0" + getDay();
        } else {
            day = String.valueOf(getDay());
        }

        return year + "-" + month + "-" + day;
    }

}
