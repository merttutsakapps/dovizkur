package com.merttutsakgmail.dovizkur.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.merttutsakgmail.dovizkur.R;
import com.merttutsakgmail.dovizkur.activities.BaseActivity;

import java.util.Calendar;
import java.util.Date;

public class SettingsFragment extends BaseFragment {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private String[] kKur;
    private String[] uKur;
    private BaseActivity baseActivity;
    private TextView temeBirimText;
    private ImageButton dovizKuruSecButton;
    private TextView dateText;
    private ImageButton dateSecButton;
    private DatePickerDialog datePickerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        baseActivity = (BaseActivity) getActivity();

        baseActivity.isExit("SETTINGS");

        preferences = super.baseActivity.getSharedPreferences("tercihlerim", Context.MODE_PRIVATE);
        editor = preferences.edit();

        kKur = getResources().getStringArray(R.array.kKur);
        uKur = getResources().getStringArray(R.array.uKur);

        temeBirimText = (TextView) view.findViewById(R.id.temeBirimText);
        dovizKuruSecButton = (ImageButton) view.findViewById(R.id.dovizKuruSecButton);
        dateText = (TextView) view.findViewById(R.id.dateText);
        dateSecButton = (ImageButton) view.findViewById(R.id.dateSecButton);

        baseActivity.midButtonText("Ayarlar");
        baseActivity.visibilityButton(true, true, false);

        dateText.setText(getDATE());

        dateSecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreatedDateTimePickerDialog().show();
            }
        });


        temeBirimText.setText(uKur[getSeciliEleman()].toString() + " (" + kKur[getSeciliEleman()].toString() + ")");

        dovizKuruSecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DOVIZKURU", "DÜZENLE TIKLANDI");
                baseActivity.cameFromSettings(getSeciliEleman(), "SETINGS_SECILI_OLMAYAN");
                setSeciliEleman(-1);
                chooseFragment(new ListFragment(), true);
            }
        });

        return view;
    }

    void setSeciliEleman(int elemanIndex) {
        editor.putInt("SECILEN", elemanIndex).apply();
    }

    int getSeciliEleman() {
        return preferences.getInt("SECILEN", -1);
    }


    void setDATE(int YEAR, int MONTH, int DAY) {
        editor.putInt("YEAR", YEAR).apply();
        editor.putInt("MONTH", MONTH + 1).apply();
        editor.putInt("DAY", DAY).apply();
    }

    int getYear() {
        return preferences.getInt("YEAR", Calendar.getInstance().get(Calendar.YEAR));
    }

    int getMonth() {
        return preferences.getInt("MONTH", Calendar.getInstance().get(Calendar.MONTH)+1);
    }

    int getDay() {
        return preferences.getInt("DAY", Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+1);
    }

    String getDATE() {
        String month;
        String day;
        String year;

        if (getYear() < 10) {
            year = "0" + getYear();
        } else {
            year = String.valueOf(getYear());
        }
        if (getMonth() < 10) {
            month = "0" + getMonth();
        } else {
            month = String.valueOf(getMonth());
        }
        if (getDay() < 10) {
            day = "0" + getDay();
        } else {
            day = String.valueOf(getDay());
        }

        return year + "-" + month + "-" + day;
    }

    DatePickerDialog CreatedDateTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(baseActivity, listener, getYear(), getMonth(), getDay());
        datePickerDialog.setCancelable(false);

        calendar.add(Calendar.YEAR, -5);
        datePickerDialog.getDatePicker().setMinDate(calendar.getTime().getTime());
        datePickerDialog.getDatePicker().setMaxDate(new  Date().getTime());

        return datePickerDialog;
    }

    DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Log.d("SETDATE", year + "-" + monthOfYear + "-" + dayOfMonth);

            setDATE(year, monthOfYear, dayOfMonth);
            dateText.setText(getDATE());

        }
    };
}
