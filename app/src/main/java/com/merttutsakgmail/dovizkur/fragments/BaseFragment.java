package com.merttutsakgmail.dovizkur.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.merttutsakgmail.dovizkur.R;
import com.merttutsakgmail.dovizkur.activities.BaseActivity;

public class BaseFragment extends Fragment {


    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private FragmentTransaction fragmentTransaction;
    protected BaseActivity baseActivity;



    private boolean WhereCameControl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseActivity = (BaseActivity) getActivity();

        preferences = baseActivity.getSharedPreferences("tercihlerim", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void chooseFragment(BaseFragment baseFragment, final boolean whereCameControl) {


        baseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setDoCameSettingsControl(whereCameControl);
            }
        });
        this.WhereCameControl = whereCameControl;
        fragmentTransaction = baseActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment, baseFragment).addToBackStack(baseFragment.toString());
        fragmentTransaction.commit();
        Log.d("STACK", "PUSH" + baseFragment.toString());

    }
    public void setDoCameSettingsControl(boolean tercih) {
        if (tercih == true) {
            Log.d("SETTINGS", "TRUE");
        } else {
            Log.d("SETTINGS", "FALSE");
        }
        editor.putBoolean("SETTINGS", tercih).apply();
    }


}