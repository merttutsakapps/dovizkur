package com.merttutsakgmail.dovizkur.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.util.Log;

import com.merttutsakgmail.dovizkur.R;

/**
 * Created by MERT on 13.06.2017.
 */

public class CustomListAdapter extends BaseAdapter {

    private Context mcontext;
    private String[] mKKurList;
    private String[] mBKurList;
    private String mSKurList;
    private double[] mOranList;

    public CustomListAdapter(Context mcontext, String[] mKKurList, String[] mBKurList, String mSKurList, double[] mOranList) {
        this.mcontext = mcontext;
        this.mBKurList = mBKurList;
        this.mKKurList = mKKurList;
        this.mOranList = mOranList;
        this.mSKurList = mSKurList;
    }

    @Override
    public int getCount() {
        return mOranList.length;
    }

    @Override
    public Object getItem(int position) {
        return mOranList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_kur, null);
        }

        TextView kKurAdText = (TextView) convertView.findViewById(R.id.kKurAdTextView);
        TextView bKurAdText = (TextView) convertView.findViewById(R.id.bKurAdTextView);
        TextView oranText = (TextView) convertView.findViewById(R.id.oranTextView);

        kKurAdText.setText(mBKurList[position]);
        bKurAdText.setText(mKKurList[position]);
        Log.d("mSKurList",mSKurList);
        oranText.setText(String.format("%.4f",
                1 / mOranList[position]) + " (" + mSKurList + ")");

        return convertView;
    }
}
